import mongoose from 'mongoose';

const SheetSchema = new mongoose.Schema({
  createdAt: Date,
  insertedAt: { type: Date, expires: '1d', default: Date.now },
  agency: { type: String, trim: true },
  type: { type: String, trim: true },
  city: { type: String, default: 'Rennes' },
  constructionYear: { type: String, trim: true },
  url: { type: String, index: true, unique: true },
  reference: { type: String, trim: true },
  status: { type: String, trim: true, default: 'FOR_SALE' },
  title: { type: String, trim: true },
  description: { type: String, trim: true },
  districts: [{ type: String, uppercase: true }],
  rooms: Number,
  surface: Number,
  fullPrice: { type: Number, default: 0 },
  parking: { type: Boolean, default: false },
  duplex: { type: Boolean, default: false },
  dpe: { type: String, trim: true },
  ges: { type: String, trim: true },
  annualCharges: { type: Number, default: 0 },
  photos: [String],
});

export const SheetModel = mongoose.model('Sheet', SheetSchema);

export type Sheet = mongoose.InferSchemaType<typeof SheetSchema> & {
  id?: string;
}; // include virtual getter "id" set by mongoose
