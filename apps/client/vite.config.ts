import { defineConfig } from 'vite';

import vue from '@vitejs/plugin-vue';
import UnoCSS from 'unocss/vite';
import { presetUno } from 'unocss';
import type { Theme } from 'unocss/preset-uno';
import presetIcons from '@unocss/preset-icons';
import presetTheme from 'unocss-preset-theme';
import transformerDirectives from '@unocss/transformer-directives';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    UnoCSS({
      theme: {
        colors: {
          primary: '#97cba9',
          'primary-border': '#97cba9',
          text: 'rgba(0, 0, 0, 0.88)',
          background: '#ececec',
          container: '#ececec',
          border: '#d9d9d9',
        },
        breakpoints: {
          xs: '475px',
          sm: '640px',
          md: '768px',
          lg: '1024px',
          xl: '1280px',
          '2xl': '1536px',
        },
      },
      transformers: [transformerDirectives()],
      presets: [
        presetUno(),
        presetIcons<Theme>({}),
        presetTheme<Theme>({
          theme: {
            dark: {
              colors: {
                primary: '#57d5be',
                'primary-border': '#57d5be',
                border: '#424242',
                background: '#0a192f',
                container: '#172a46',
                text: '#a7b1d0',
              },
            },
          },
        }),
      ],
    }),
  ],
  build: {
    outDir: '../../dist/apps/client',
  },
  server: {
    proxy: {
      '/graphql': {
        target: 'http://localhost:3333/graphql',
        changeOrigin: true,
        secure: false,
      },
    },
  },
});
