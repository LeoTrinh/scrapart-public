import { Ref, ref } from 'vue';
import { defineStore } from 'pinia';
import { Sheet } from '@scrapart/models';

import { getSheets, removeSheet } from '../api';
import { useAsyncWrapper } from '../composables/useAsyncWrapper';

export const useSheetsStore = defineStore('sheets', () => {
  const sheets: Ref<Sheet[]> = ref([]);

  const fetchSheetsAction = useAsyncWrapper<typeof getSheets>(
    getSheets,
    (results) => {
      sheets.value = sheets.value.concat(results);
    }
  );

  const removeSheetAction = useAsyncWrapper<typeof removeSheet>(
    removeSheet,
    (result, removedId) => {
      const index = sheets.value.findIndex((sheet) => sheet.id === removedId);
      sheets.value.splice(index, 1);
    }
  );

  return {
    sheets,
    fetchSheets: fetchSheetsAction,
    removeSheet: removeSheetAction,
  };
});
