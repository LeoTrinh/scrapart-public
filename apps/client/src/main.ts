import { createApp } from 'vue';
import { createPinia } from 'pinia';
import './style.css';
import 'uno.css';

import router from './router';
import App from './App.vue';
import EmptyLayout from './layouts/EmptyLayout.vue';
import DefaultLayout from './layouts/DefaultLayout.vue';

const app = createApp(App);

app.component('EmptyLayout', EmptyLayout);
app.component('DefaultLayout', DefaultLayout);

app.use(router);

const pinia = createPinia();
app.use(pinia);

app.mount('#app');
