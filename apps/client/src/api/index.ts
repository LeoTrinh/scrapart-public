import { Sheet } from '@scrapart/models';
import { request, gql } from 'graphql-request';

export const getSheets = async (page = 1, count = 10): Promise<Sheet[]> => {
  const query = gql`
    query sheets($page: Int, $count: Int) {
      sheets(page: $page, count: $count) {
        id
        createdAt
        inserteddAt
        agency
        type
        city
        constructionYear
        url
        reference
        status
        title
        description
        districts
        rooms
        surface
        fullPrice
        parking
        duplex
        dpe
        ges
        annualCharges
        photos
      }
    }
  `;

  const result = await request<{ sheets: Sheet[] }>('/graphql', query, {
    page,
    count,
  });

  return result.sheets;
};

export const removeSheet = async (id: string): Promise<boolean> => {
  const mutation = gql`
    mutation removeSheet($id: String!) {
      remove_sheet(id: $id)
    }
  `;

  const result = await request<{ removeSheet: boolean }>('/graphql', mutation, {
    id,
  });

  return result.removeSheet;
};
