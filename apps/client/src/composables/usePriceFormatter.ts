export function usePriceFormatter() {
  return (price: number | undefined) =>
    new Intl.NumberFormat('fr-FR', {
      style: 'currency',
      currency: 'EUR',
      maximumFractionDigits: 0,
    }).format(price ?? NaN);
}
