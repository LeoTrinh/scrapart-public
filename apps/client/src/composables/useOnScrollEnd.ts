export function onScrollEnd(e: UIEvent, cb: () => Promise<void>) {
  const { scrollTop, clientHeight, scrollHeight } = e.target as any;

  if (scrollTop + clientHeight + 1 >= scrollHeight) {
    cb();
  }
}
