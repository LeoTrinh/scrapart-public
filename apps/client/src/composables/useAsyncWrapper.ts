/* eslint-disable @typescript-eslint/no-explicit-any */
import { Ref, ref } from 'vue';

export const useAsyncWrapper =
  <F extends (...args: any[]) => any>(
    request: F,
    handle: (res: Awaited<ReturnType<F>>, ...args: Parameters<F>) => void
  ) =>
  (...args: Parameters<F>) => {
    const isLoading = ref(false);
    const result: Ref<Awaited<ReturnType<F>> | null> = ref(null);
    const error: Ref<Error | null> = ref(null);

    const execute = async (...args: Parameters<F>) => {
      isLoading.value = true;
      error.value = null;

      try {
        const response = await request(...args);
        await handle(response, ...args);

        result.value = response;
        return result.value;
      } catch (e) {
        if (e instanceof Error) error.value = e;
        result.value = null;
      } finally {
        isLoading.value = false;
      }
    };

    // Launch request but do not await
    execute(...args);

    return {
      isLoading,
      result,
      error,
    };
  };
