import { reactive } from 'vue';

export default function useDarkTheme() {
  const theme = reactive({
    dark: localStorage.getItem('dark') === 'true',
  });

  const switchTheme: () => void = () => {
    localStorage.setItem('dark', `${!theme.dark}`);
    theme.dark = !theme.dark;
  };

  return {
    theme,
    switchTheme,
  };
}
