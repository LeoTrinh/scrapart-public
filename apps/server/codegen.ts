import type { CodegenConfig } from '@graphql-codegen/cli';

const config: CodegenConfig = {
  schema: './src/models/graphql/**/*.gql',
  generates: {
    './generated/resolvers-types.ts': {
      config: {
        useIndexSignature: true,
        mappers: {
          Sheet: '../src/models#SheetDB',
        },
      },
      plugins: ['typescript', 'typescript-resolvers'],
    },
  },
};
export default config;
