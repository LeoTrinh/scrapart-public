import { connect } from 'mongoose';

import config from '../config';
import logger from './logger';

export async function initDatabaseConnection(retry = 0) {
  logger.info('Trying to connect to database');
  return new Promise((resolve, reject) => {
    tryConnect()
      .catch((err) => {
        logger.error(err, 'Error while trying to connect to database');
        if (retry > 3) reject(new Error('Unable to connect to database'));
        setTimeout(() => resolve(initDatabaseConnection(++retry)), 3000);
      })
      .then(resolve);
  });
}

async function tryConnect() {
  await connect(
    `mongodb://${config.db.host}:${config.db.port}/${config.db.database}`
  );
}
