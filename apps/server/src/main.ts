import express from 'express';
import * as path from 'path';
import { createYoga, createSchema } from 'graphql-yoga';

import config from './config';
import schema from './schema';
import resolvers from './resolvers';
import { initDatabaseConnection } from './infrastructure/database';
import logger from './infrastructure/logger';

const port = process.env.PORT || 3333;

(async function () {
  const app = express();

  const context = {
    config,
    logger,
  };

  await initDatabaseConnection();

  const executableSchema = createSchema<typeof context>({
    typeDefs: schema,
    resolvers,
  });

  const yoga = createYoga({
    schema: executableSchema,
    context,
  });

  app.use('/', express.static(path.join(__dirname, '..', 'client')));
  app.use('/graphql', yoga);

  const server = app.listen(port, () => {
    logger.info(
      `Running a GraphQL API server at http://localhost:${port}/graphql`
    );
  });

  server.on('error', logger.error);
})();
