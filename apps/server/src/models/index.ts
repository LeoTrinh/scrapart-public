import { Logger } from 'pino';
import { Sheet } from '@scrapart/models';

import { Config } from './config';

export * from './config';

export type Context = {
  config: Config;
  logger: Logger;
};

export type SheetDB = Sheet;
