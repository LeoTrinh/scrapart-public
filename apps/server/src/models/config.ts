import { ConfigSchema } from '../config';

export type Config = Zod.infer<typeof ConfigSchema>;
