import { z } from 'zod';

export const ConfigSchema = z.object({
  db: z.object({
    host: z.string().default('localhost'),
    port: z.number().default(27017),
    database: z.string().default('scrapart'),
  }),
});

export default ConfigSchema.parse({
  db: {
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT),
    database: process.env.DB_DATABASE,
  },
});
