import { SheetModel } from '@scrapart/models';
import { Context } from '../models';
import { Resolvers } from '../../generated/resolvers-types';

const resolvers: Resolvers<Context> = {
  Query: {
    sheets: async (_root, { page = 1, count = 20 }) => {
      return await SheetModel.find()
        .sort({ insertedAt: 'desc' })
        .skip(Math.max((page - 1) * count, 0))
        .limit(count)
        .exec();
    },
  },
  Mutation: {
    remove_sheet: async (_root, { id }) => {
      return Boolean(await SheetModel.findByIdAndDelete(id).exec());
    },
  },
};

export default resolvers;
