const queries = /* GraphQL */ `
  type Query {
    sheets(page: Int, count: Int): [Sheet]
  }
`;

const mutations = /* GraphQL */ `
  type Mutation {
    remove_sheet(id: String!): Boolean!
  }
`;

const types = /* GraphQL */ `
  type Sheet {
    id: String!
    createdAt: String
    inserteddAt: String
    agency: String
    type: String
    city: String
    constructionYear: String
    url: String
    reference: String
    status: String
    title: String
    description: String
    districts: [String]
    rooms: Int
    surface: Int
    fullPrice: Int
    parking: Boolean
    duplex: Boolean
    dpe: String
    ges: String
    annualCharges: Int
    photos: [String]
  }
`;

export default [queries, mutations, types];
