import { Sheet, SheetProps } from './models';

export const numberInText = (s: string) => {
  const matched = s?.match(/\d[\d\s]*/)?.[0];
  return Number.parseInt(matched?.replace(/\s/, ''));
};

export const dpeEnrichment = (description: string) => {
  const regex = /classe\s+([É�é](nergie|nergétique)).*\b(?<class>\w{1})\b/gi;
  return regex.exec(description)?.groups['class'] ?? '';
};

export const gesEnrichment = (description: string) => {
  const regex = /classe\s+(climat).*\b(?<class>\w{1})\b/gi;
  return regex.exec(description)?.groups['class'] ?? '';
};

export const parkingEnrichment = (description: string): boolean =>
  !!description.match(
    /(parking|garage|box|stationnement)s?\s+((au|en) sous[ -]sol|ferm[eé]|couvert|privé|privati(f|ve)|commun|a[eé]rien|attenant|individuel)/i
  );

export const duplexEnrichment = (description: string): boolean =>
  !!description.match(/(duplex)/i);

export const annualChargesEnrichment = (description: string) =>
  Number.parseInt(
    description
      .match(/charges.*?(?<x>\d(?:\d|\s)+)/i)
      ?.groups?.['x'].replace(' ', '')
  ) || 0;

export const constructionYearEnrichment = (description: string) =>
  description.match(/(ann[ée]{2}s?)\s(?<year>\d{2,4})/gi)?.groups?.['year'] ??
  '';

export const districtEnrichment = (description: string) => {
  const districtRegex = /(quartier|secteur)\s+(?<district>\w+)/gi;

  const districts = description.match(districtRegex)?.groups?.['district'];
  return districts ? [districts] : [];
};

const enrichmentMapping: Partial<
  Record<SheetProps, (description: string) => unknown>
> = {
  dpe: dpeEnrichment,
  ges: gesEnrichment,
  parking: parkingEnrichment,
  duplex: duplexEnrichment,
  annualCharges: annualChargesEnrichment,
};

export const enrichSheetFromDescription = (
  sheet: Partial<Sheet>,
  description: string
) =>
  Object.entries(enrichmentMapping).reduce(
    (result, [prop, enrichmentFn]) => ({
      ...result,
      [prop]: enrichmentFn(description),
    }),
    sheet
  );
