import { Sheet, SheetModel } from '@scrapart/models';

type SheetProps = { [K in keyof Sheet]: K }[keyof Sheet];

export { SheetModel };
export type { Sheet, SheetProps };
