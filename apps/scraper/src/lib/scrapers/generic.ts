import axios, { Axios } from 'axios';
import retry from 'axios-retry';
import rateLimit from 'axios-rate-limit';
import * as cheerio from 'cheerio';

import { Sheet } from '../models';
import { enrichSheetFromDescription } from '../enrichment';

const defaultAxios = rateLimit(axios.create(), {
  maxRPS: 20,
});
retry(defaultAxios, {
  retries: 2,
});

export default abstract class GenericScraper {
  name: string;
  domainUrl: string;
  scrapUrl: string;
  numberOfPages: number;
  http: Axios;

  constructor(
    name: string,
    domainUrl: string,
    scrapUrl: string,
    numberOfPages: number,
    http?: Axios
  ) {
    this.name = name;
    this.domainUrl = domainUrl;
    this.scrapUrl = scrapUrl;
    this.numberOfPages = numberOfPages;
    this.http = http ?? defaultAxios;
  }

  async run() {
    const pages = Array.from({ length: this.numberOfPages }, (_, i) => i + 1);
    const results = await Promise.all(
      pages.map((n) => this.scrapPage(this.pageUrl(n)))
    );

    const flatted = results.flat();
    console.log(`[${this.name}] Scrapped ${flatted.length} sheets`);

    return flatted;
  }

  async scrapPage(url: string, linksSelector = ''): Promise<Partial<Sheet>[]> {
    const rawData = (await this.http.get(url)).data;
    const $ = cheerio.load(rawData);

    const sheetsLinks = $(linksSelector)
      .map((_, a) => $(a).attr('href'))
      .toArray();

    // Extract data from sheet details page
    const results = await Promise.all(
      sheetsLinks.map((link) => this.scrapSheet(this.domainUrl + link))
    );

    // Enrich sheet with data extracted from description
    return results.map((sheet) => this.enrichSheet(sheet));
  }

  enrichSheet(sheet: Partial<Sheet>): Partial<Sheet> {
    return enrichSheetFromDescription(sheet, sheet.description);
  }

  pageUrl(page: number): string {
    return `${this.scrapUrl}?page=${page}`;
  }

  abstract scrapSheet(url: string): Promise<Partial<Sheet>>;
}
