import { Sheet, SheetModel } from './models';

import scrapers from './scrapers';

export const scrap = async () => {
  // Run scrapers
  const results = await Promise.allSettled(
    scrapers.map((scraper) => scraper.run())
  );

  logFailed(results);

  const successResults = results
    .filter((result) => result.status == 'fulfilled')
    .map((result) => (result as PromiseFulfilledResult<any>).value)
    .flat();

  // Insertion in database
  await saveSheets(successResults);
};

export const logFailed = (results: PromiseSettledResult<unknown>[]) => {
  results
    .filter((result) => result.status == 'rejected')
    .forEach((result) => {
      console.log(
        '[Scraper] Error : ',
        (result as PromiseRejectedResult).reason
      );
    });
};

export const saveSheets = async (sheets: Sheet[]) => {
  await Promise.all(sheets.map((s) => saveSheet(s)));
};

export const saveSheet = async (sheet: Sheet) => {
  try {
    await SheetModel.findOneAndUpdate(
      {
        url: sheet.url,
      },
      sheet,
      { upsert: true }
    ).exec();
  } catch (err) {
    console.log('[Scraper] Error while saving sheet :', err);
  }
};
