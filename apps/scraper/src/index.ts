import mongoose from 'mongoose';
import config from './config';

import { scrap } from './lib/scraper';

(async function run() {
  console.log('Running');

  await initDatabase();

  await scrap();

  console.log('Done');
})();

async function initDatabase() {
  const { protocol, user, password, host, port, database } = config.db;
  const auth = user && password ? `${user}:${password}@` : '';

  await mongoose.connect(`${protocol}://${auth}${host}:${port}/${database}`);
}
