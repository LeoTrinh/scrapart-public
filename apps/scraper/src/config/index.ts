export default {
  app: {
    name: 'scrapart/scraper',
  },
  db: {
    host: process.env['DB_HOST'],
    user: process.env['DB_USER'],
    password: process.env['DB_PASSWORD'],
    port: process.env['DB_PORT'],
    database: process.env['DB_DATABASE'] ?? 'scrapart',
    protocol: process.env['PROTOCOL'] ?? 'mongodb',
  },
};
