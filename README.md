# Scrapart

![Demo](./docs/scrapart_demo.gif)

## What is it

A little personal project designed with 2 main goals in mind : find an appartment and experiment with some new technologies.  
It consists of a CRON component scraping some real estate websites and storing them into a database. The data is then exposed through a simple GraphQL API, and consumed by the web client application.
For legal reasons, the actual scrapers are not visible in this repo.

## Stack

- Nx monorepo
- Typescript & Node
- Mongo database & Mongoose
- Server
  - GraphQL Yoga API
  - Express (API + serving static)
- Client
  - Vue 3 (Composition API)
  - Vite
  - Pinia
  - UnoCSS
- Docker & Docker-Compose

## Understand this workspace

Run `nx graph` to see a diagram of the dependencies of the projects.
